#ifndef SHADERS_H
#define SHADERS_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shaders
{
public:
	GLuint Program;

	//Constructor should generate shader at object's creation.
	Shaders(GLchar* vertexPath, GLchar* fragmentPath)
	{
		//Step 1: retrieve shader code from the .txt files

		//creating variables that'll act sorta like bins and waystations
		std::string vertexCode;
		std::string fragmentCode;
		//creating inputStream objects
		std::ifstream vertexFile;
		std::ifstream fragmentFile;
		//ensures ifstream objects can throw exceptions
		vertexFile.exceptions(std::ifstream::badbit);
		fragmentFile.exceptions(std::ifstream::badbit);

		try
		{
			//Open the got dang files
			vertexFile.open(vertexPath);
			fragmentFile.open(fragmentPath);
			std::stringstream vShaderStream, fShaderStream;
			//Read the buffer contents into the streams...
			vShaderStream << vertexFile.rdbuf();
			fShaderStream << fragmentFile.rdbuf();
			//Close thd files
			vertexFile.close();
			fragmentFile.close();
			//convert the stream into a string
			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();
		}
		catch(std::ifstream::failure e)
		{
			std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		}
		const GLchar *vShaderCode = vertexCode.c_str();
		const GLchar *fShaderCode = fragmentCode.c_str();

		//Step 2: Compiling the Shaders
		//Variables for compiling the shaders
		GLuint vertex, fragment;
		GLint success;
		GLchar infoLog[512];

		//Vertex Shader Compilation
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);

		//Checking for compile errors...
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		//Fragment Shader Compilation
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);

		//Checking for compile errors again...
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		//Now we actually form the shader program...
		this->Program = glCreateProgram();
		GLuint shaderProgram = this->Program;
		glAttachShader(this->Program, vertex);
		glAttachShader(this->Program, fragment);
		//Link the program
		glLinkProgram(this->Program);
		

		// Print linking errors if any
		glGetProgramiv(this->Program, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(this->Program, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
		}
		//Delete shaders because they have been linked to our program and aren't necessary
		glDeleteShader(vertex);
		glDeleteShader(fragment);

	}
	//This function uses the current shader
	void Use()
	{
		glUseProgram(this->Program);
	}
	GLint get_Attrib(const char* attrib)
	{
		return glGetAttribLocation(this->Program, attrib);
	}
	
	void setUniform1f(const GLchar* name, float value)
	{
		glUniform1f(glGetUniformLocation(this->Program,name), value);
	}
	void setUniform1i(const GLchar* name, int value)
	{
		glUniform1i(glGetUniformLocation(this->Program, name), value);
	}
	void setUniform1fv(const GLchar* name, float* value, int count )
	{
		glUniform1fv(glGetUniformLocation(this->Program, name), count, value);
	}
	void setUniform1iv(const GLchar* name, int* value, int count)
	{
		glUniform1iv(glGetUniformLocation(this->Program, name), count, value);
	}
	void setUniform2f(const GLchar* name, const glm::vec2& vector)
	{
		glUniform2f(glGetUniformLocation(this->Program, name), vector[0], vector[1]);
	}
	void setUniform3f(const GLchar* name, const glm::vec3& vector)
	{
		glUniform3f(glGetUniformLocation(this->Program, name), vector[0], vector[1], vector[2]);
	}
	void setUniform4f(const GLchar* name, const glm::vec4& vector)
	{
		glUniform4f(glGetUniformLocation(this->Program, name), vector[0], vector[1], vector[2], vector[3]);
	}
	void setUniformMat4(const GLchar* name, const glm::mat4& matrix)
	{

		float elements[16] = { 0.0 };

		const float *pSource = (const float*)glm::value_ptr(matrix);
		for (int i = 0; i < 16; ++i)
			elements[i] = pSource[i];

		glUniformMatrix4fv(glGetUniformLocation(this->Program, name), 1, GL_FALSE, elements);
	}
private:

};

#endif
