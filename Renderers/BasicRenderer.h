#pragma once

#include "Renderer2D.h"
#include <deque>
#include "../Renderables/StaticSprite.h"

class BasicRenderer : public Renderer2D
{
private:
	std::deque< const StaticSprite*> _RenderQueue;
public:
	void submit(const Sprite* sprite) override;
	void flush() override;
};