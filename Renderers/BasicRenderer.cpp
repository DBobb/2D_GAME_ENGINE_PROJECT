#include "BasicRenderer.h"
#include "../Renderables/Sprite.h"

void BasicRenderer::submit(const Sprite* sprite)
{
	_RenderQueue.push_back((StaticSprite*) sprite);
}
void BasicRenderer::flush()
{
	while (!_RenderQueue.empty())
	{
		const StaticSprite* sprite = _RenderQueue.front();
		sprite->getVAO()->bind();
		sprite->getEBO()->bind();

		glm::mat4 projection(1);
		glm::mat4 model(1);
		glm::mat4 view(1);

		view = _TransformationStack.back() * glm::translate(view, glm::vec3(sprite->getPosition())); //NOTE: HERE I CONVERT THE VEC4 INTO A VEC3
		projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, -1.0f, 1.0f); //NOTE: 800 and 600 refer to the size of the coordinate space we're defining.

		sprite->getShader().setUniformMat4("view", view);
		sprite->getShader().setUniformMat4("projection", projection);
		sprite->getShader().setUniformMat4("model", model);

		GLuint texID = sprite->getTexBufferID(); 
		glBindTexture(GL_TEXTURE_2D, texID);

		
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		
		sprite->getVAO()->unbind();
		sprite->getEBO()->unbind();

		_RenderQueue.pop_front();
	}
}