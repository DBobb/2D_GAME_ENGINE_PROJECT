﻿#include "BatchRenderer.h"
//#include "../Renderables/Sprite.h"
BatchRenderer::BatchRenderer()
{
	init();
}
BatchRenderer::~BatchRenderer()
{
	delete _ebo;
	glDeleteBuffers(1, &_gbo);
}
void BatchRenderer::init()
{
	glGenVertexArrays(1, &_vao);
	glGenBuffers(1, &_gbo);

	glBindVertexArray(_vao);
	glBindBuffer(GL_ARRAY_BUFFER, _gbo);
	glBufferData(GL_ARRAY_BUFFER, RENDERER_BUFFER_SIZE, NULL, GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(SHADER_VERTEX_INDEX);
	glEnableVertexAttribArray(SHADER_COLOR_INDEX);
	glEnableVertexAttribArray(SHADER_TEXCOORD_INDEX);
	glEnableVertexAttribArray(SHADER_TEXID_INDEX);
	glEnableVertexAttribArray(SHADER_TEXT_INDEX);

	glVertexAttribPointer(SHADER_VERTEX_INDEX, 2, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*) 0);
	glVertexAttribPointer(SHADER_TEXCOORD_INDEX, 2, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)(offsetof(VertexData, VertexData::_texCoords)));
	glVertexAttribPointer(SHADER_TEXID_INDEX, 1, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)(offsetof(VertexData, VertexData::_texID)));
	glVertexAttribPointer(SHADER_COLOR_INDEX, 4, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)(offsetof(VertexData, VertexData::_color))); //NOTE OFFSET MIGHT BE DIFFERENT DUE TO GLM SIZE OF VECTORS
	glVertexAttribPointer(SHADER_TEXT_INDEX, 1, GL_FLOAT, GL_FALSE, RENDERER_VERTEX_SIZE, (const GLvoid*)(offsetof(VertexData, VertexData::_text)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	GLint elements[RENDERER_ELEMENTS_SIZE];

	int offset = 0;
	for (int i = 0; i < RENDERER_ELEMENTS_SIZE; i+= 6)
	{
		elements[i] =	  offset + 0;
		elements[i + 1] = offset + 1;
		elements[i + 2] = offset + 2;

		elements[i + 3] = offset + 2;
		elements[i + 4] = offset + 3;
		elements[i + 5] = offset + 0;

		offset += 4;
	}

	_ebo = new ElementBuffer(elements, RENDERER_ELEMENTS_SIZE);

	glBindVertexArray(0);

	_FTatlas = ftgl::texture_atlas_new(512, 512, 1);
	_FTfont = ftgl::texture_font_new_from_file(_FTatlas, 35, "comicbd.ttf");

	glGenTextures(1, &_FTatlas->id);



}

void BatchRenderer::submit(const Sprite* sprite)
{
	

	const glm::vec4& position = sprite->getPosition();
	const glm::vec4& color = sprite->getColor();
	const glm::vec2& size = sprite->getSize();
	const std::vector<glm::vec2>& texcoords = sprite->getTexCoords();
	GLuint texID = sprite->getTexID();
	//Done 4 times for each vertex


	float ts = 0.0f;
	bool found = false;
	for (int i = 0; i < _TextureSlots.size(); i++)
	{
		if (_TextureSlots[i] == texID)
		{
			ts = (float)(i + 1);
			found = true;
			break;

		}
	}

	if (!found)
	{
		if (_TextureSlots.size() >= 32)
		{
			end();
			flush();
			begin();
		}
		_TextureSlots.push_back(texID);
		ts = (float)(_TextureSlots.size());

	}

	_buffer->_text = 0.0f;

	_buffer->_vertex = *_TransformBack * position;
	_buffer->_texCoords = texcoords[0];
	_buffer->_texID = ts;
	_buffer->_color = color;
	_buffer++;

	_buffer->_vertex = *_TransformBack * glm::vec4(position[0], position[1] + size[1], position[2], position[3]);
	_buffer->_texCoords = texcoords[1];
	_buffer->_texID = ts;
	_buffer->_color = color;
	_buffer++;

	_buffer->_vertex = *_TransformBack * glm::vec4(position[0] + size[0], position[1] + size[1], position[2], position[3]);
	_buffer->_texCoords = texcoords[2];
	_buffer->_texID = ts;
	_buffer->_color = color;
	_buffer++;

	_buffer->_vertex = *_TransformBack * glm::vec4(position[0] + size[0], position[1], position[2], position[3]);
	_buffer->_texCoords = texcoords[3];
	_buffer->_texID = ts;
	_buffer->_color = color;
	_buffer++;


	_ElementCount += 6;
}


void BatchRenderer::drawString(const std::string& text, const glm::vec4& position, const Font& font, const glm::vec4& color)
{
	

	using namespace ftgl;

	float ts = 0.0f;
	bool found = false;
	for (int i = 0; i < _TextureSlots.size(); i++)
	{
		if (_TextureSlots[i] == font.getID())
		{
			ts = (float)(i + 1);
			found = true;
			break;

		}
	}

	if (!found)
	{
		if (_TextureSlots.size() >= 32)
		{
			end();
			flush();
			begin();
		}
		_TextureSlots.push_back(font.getID());
		ts = (float)(_TextureSlots.size());

	}
	
	texture_font_t* ft_font = font.getFTFont();
	std::string testing = "DATTEBAYO";

	//texture_font_get_glyph(_FTfont, &testing[0]);
	
	float x = position.x;
	
	for (int i = 0; i < text.length(); i++)
	{
		 char c = text[i];

		texture_glyph_t* glyph = texture_font_get_glyph(ft_font, c);
		
		texture_font_get_glyph(_FTfont, c);
		if (glyph != NULL)
		{

			if (i > 0)
			{
				float kerning = texture_glyph_get_kerning(glyph, text[i - 1]);
				x += kerning;
			}

			float x0 = x + glyph->offset_x;
			float y0 = position.y + glyph->offset_y;
			float x1 = x0 + glyph->width;
			float y1 = y0 - glyph->height;

			float u0 = glyph->s0;
			float v0 = glyph->t0;
			float u1 = glyph->s1;
			float v1 = glyph->t1;


			_buffer->_text = 1.0f;
			

			_buffer->_vertex = *_TransformBack * glm::vec4(x0, y0, 0, 1);
			_buffer->_texCoords = glm::vec2(u0, 1.0f-v0);
			_buffer->_color = color;
			_buffer->_texID = ts;
	
			_buffer++;

			_buffer->_vertex = *_TransformBack * glm::vec4(x0, y1, 0, 1);
			_buffer->_texCoords = glm::vec2(u0, 1.0f-v1);
			_buffer->_color = color;
			_buffer->_texID = ts;

			_buffer++;

			_buffer->_vertex = *_TransformBack * glm::vec4(x1, y1, 0, 1);
			_buffer->_texCoords = glm::vec2(u1, 1.0f-v1);
			_buffer->_color = color;
			_buffer->_texID = ts;

			_buffer++;

			_buffer->_vertex = *_TransformBack * glm::vec4(x1, y0, 0, 1);
			_buffer->_texCoords = glm::vec2(u1, 1.0f-v0);
			_buffer->_color = color;
			_buffer->_texID = ts;

			_buffer++;
			
			
			_ElementCount += 6;	

			x += glyph->advance_x;

		}


	}

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//glBindTexture(GL_TEXTURE_2D, _FTatlas->id);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _FTatlas->width, _FTatlas->height, 0, GL_RED, GL_UNSIGNED_BYTE, _FTatlas->data);//*/
}


void BatchRenderer::begin()
{
	glBindBuffer(GL_ARRAY_BUFFER, _gbo);
	_buffer = (VertexData*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
}

void BatchRenderer::end()
{
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


}

void BatchRenderer::flush()
{

	for (int i = 0; i < _TextureSlots.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, _TextureSlots[i]);	


	}
	
	
	
	glBindVertexArray(_vao);
	_ebo->bind();

	//GLuint texID = sprite->getTexBufferID();
	//glBindTexture(GL_TEXTURE_2D, _texID);

	glDrawElements(GL_TRIANGLES, _ElementCount, GL_UNSIGNED_INT, NULL);

	_ebo->unbind();
	glBindVertexArray(0);

	_ElementCount = 0;
}