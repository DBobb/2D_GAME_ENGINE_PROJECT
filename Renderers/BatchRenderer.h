#pragma once

#include "Renderer2D.h"
#include "../Renderables/Sprite.h"
#include "../Buffers/ElementBuffer.h"
#include <cstddef>
//#include "../font.h"
//#include "../External Libraries/Freetype-gl/freetype-gl-master/freetype-gl.h"

#define MAX_SPRITES 30000
#define RENDERER_VERTEX_SIZE		sizeof(VertexData)
#define RENDERER_SPRITE_SIZE		RENDERER_VERTEX_SIZE * 4
#define RENDERER_BUFFER_SIZE		RENDERER_SPRITE_SIZE * MAX_SPRITES
#define RENDERER_ELEMENTS_SIZE		MAX_SPRITES * 6

#define SHADER_VERTEX_INDEX   0
#define SHADER_TEXCOORD_INDEX 1
#define SHADER_TEXID_INDEX	  2
#define SHADER_COLOR_INDEX    3
#define SHADER_TEXT_INDEX	  4

class BatchRenderer : public Renderer2D
{
private:
	GLuint _vao;
	GLuint _gbo;
	ElementBuffer* _ebo;
	VertexData* _buffer;
	GLsizei _ElementCount = 0;

	std::vector<GLuint> _TextureSlots;
	ftgl::texture_atlas_t* _FTatlas;
	ftgl::texture_font_t* _FTfont;

	void init();
public:
	BatchRenderer();
	~BatchRenderer();
	void submit(const Sprite* sprite) override;
	void drawString(const std::string& text, const glm::vec4& position, const Font& font, const glm::vec4& color) override;
	void flush() override;
	void begin() override;
	void end() override;
};