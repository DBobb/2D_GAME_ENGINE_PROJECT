#pragma once
//#include "../Renderables/Sprite.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include "../font.h"


class Sprite;

class Renderer2D
{
protected:
	std::vector<glm::mat4> _TransformationStack;
	const glm::mat4* _TransformBack;
protected:
	//Making sure that our transformation stack is initialized with the identity matrix.
	Renderer2D()
	{
		_TransformationStack.push_back(glm::mat4(1));
		_TransformBack = &_TransformationStack.back();
	}
public:
	virtual void begin() {};
	virtual void submit(const Sprite* sprite) = 0;
	virtual void drawString(const std::string& text, const glm::vec4& position, const Font& font, const glm::vec4& color) {}
	virtual void flush() = 0;
	virtual void end() {};

	void stackPop()
	{
		if (_TransformationStack.size() > 1)
		{
			_TransformationStack.pop_back();
		}

		_TransformBack = &_TransformationStack.back();
	}

	void stackPush(glm::mat4 matrix, bool override = false)
	{
		if (override)
			_TransformationStack.push_back(matrix);
		else
			_TransformationStack.push_back(_TransformationStack.back() * matrix);

		_TransformBack = &_TransformationStack.back();
	}
};