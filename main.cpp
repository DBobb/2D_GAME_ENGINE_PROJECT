#include <iostream>

//SOIL
#include "SOIL2\SOIL2.h"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>
//GLM

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "glm/ext.hpp"
#include <glm/gtc/type_ptr.hpp>

#include<thread>
//Shader Files
#include "Shaders.h"
#include "window.h"

#include "Renderables/StaticSprite.h"
#include "Renderables/StdSprite.h"
#include "Renderers/BatchRenderer.h"

#include "Layers/TileLayer.h"

#include<time.h>
#include "Utils\Timer.h"

#include  "Layers/Group.h"
#include "Texture.h"
#include "Label.h"
#include "font_manager.h"

#define Updates_Per_Second 60
#define TEST_50K_SPRITES 0

int main()
{

	window newWindow("Window Class Test", 800, 600);

	double lastTime = glfwGetTime();
	double deltaTime = 0.0f;
	


	//view = glm::translate(view, glm::vec3(sprite->getPosition())); //NOTE: HERE I CONVERT THE VEC4 INTO A VEC3

	

	Shaders blah("Shaders/VertexShader2.sh", "Shaders/FragShader2.sh");
	Shaders text("Shaders/VertexShader2.sh", "Shaders/FragShaderText.sh");
	GLint texIDs[] =
	{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	};

	blah.Use();
	blah.setUniform1iv("textures", texIDs, 10);

	glm::mat4 projection(1);
	glm::mat4 model(1);
	glm::mat4 view(1);

	glm::vec4 roxyPosition = glm::vec4(100.0f, 100.0f, 0.0f, 1.0f);
	glm::vec2 size = glm::vec2(10, 10);

	Texture* texture = new Texture("resources/quote.png");
	Texture* yaboi = new Texture("resources/a_dog.png");
	Texture* character = new Texture("resources/ness_peace.png");
	Texture* robot = new Texture("resources/quote.png");

	GLuint texA_ID = newWindow.loadAndBufferImage("resources/roxy.png");
	GLuint texB_ID = newWindow.loadAndBufferImage("resources/quote.png");

	//StaticSprite roxy(glm::vec2(100.0f, 100.0f), glm::vec4(1.0f, 0.0f, 1.0f,0.5f), roxyPosition, texture.getID, blah);
	//StaticSprite quote(glm::vec2(75, 75), glm::vec4(1.0, 1.0, 1.0,0.5), glm::vec4(450, 300, 0, 1), texB_ID, blah);

	//StaticSprite roxy(100, 100, 100, 100, glm::vec4(1.0, 0.0, 0.0, 1.0), texture.getID(), blah);
	//StaticSprite quote(300, 500, 75, 75, glm::vec4(0.0, 0.0, 1.0, 1.0), texB_ID, blah);

	BatchRenderer renderer;

	Group* group = new Group(glm::translate(glm::vec3(50, 25, 0)));
	//group->add(new StdSprite(50, 50, 500, 200, glm::vec4(0.8, 0.5, 0.3, 1.0)));
	//group->add(new StdSprite(0, 0, 600, 300, glm::vec4(0.5, 0.1, 0.3, 1.0)));
	

	
	

	projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, -1.0f, 1.0f); //NOTE: 800 and 600 refer to the size of the coordinate space we're defining.

	blah.setUniformMat4("view", view);
	blah.setUniformMat4("projection", projection);
	blah.setUniformMat4("model", model);

	TileLayer layer(&blah);

	TileLayer stuff(&blah);

#if TEST_50K_SPRITES
	//SUPER FUN BATCH RENDER TEST
	for (float y = 0; y < 600.0f; y += 5.0)
	{
		for (float x = 0; x < 800.0f; x += 5.0)
		{
			//layer.add(new StdSprite(x, y, 50.0f, 50.0f, glm::vec4( rand() % 1000 / 1000.0f,1, 0, 1)));
			layer.add(new StdSprite(x, y, 5.0f, 5.0f, rand() % 2 == 0 ? texture : yaboi));

		}
	}
#endif
	layer.add(group);

	FontManager::add(new Font("Arial", "arial.ttf", 35));
	FontManager::add(new Font("ComicSans", "comicbd.ttf", 35));

	


	Label* fps = new Label("FPS", 150, 400, "Ariadfdasl", glm::vec4(1, 0, 1, 1));
	layer.add(fps);
	
	stuff.add(new StdSprite(200, 200, 175, 175, character));
	stuff.add(new StdSprite(200, 200, 75, 75, robot));
	//layer.add(new StdSprite(50, 50, 100, 100, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)));



	

	Timer time; //intializing timer object
	float timer = 0;
	int f = 0;
	unsigned int frames = 0;

	glfwSwapInterval(0.0);  // Uncomment to get uncapped frames per second count
	while (!newWindow.close())
	{
		

		newWindow.clear(0.0, 0.0, 0.0); //.2 .3 .3
		//MOUSE SPOTLIGHT FUN TIME
		glm::vec2 mousePos;
		mousePos = newWindow.getMousePosition();
		blah.setUniform2f("light_pos", glm::vec2(mousePos[0] * 800.0f / newWindow.getWidth() , mousePos[1] * 600.0f / newWindow.getHeight()));

		
		if (newWindow.isMouseButtonClicked(GLFW_MOUSE_BUTTON_LEFT))
			std::cout << "Clicked!" << std::endl;

		if (newWindow.isMouseButtonClicked(GLFW_MOUSE_BUTTON_LEFT))
			std::cout << "Back at it again!" << std::endl;

		
		stuff.draw();
		layer.draw();
		
		
		
		renderer.begin();
//		renderer.submit(&roxy);
		
//		renderer.submit(&quote);
		
		renderer.end();
		renderer.flush();

		

		newWindow.update();

		frames++;
		if (time.elapsed() - timer > 1.0f)
		{
			timer += 1.0f;
			fps->text = std::to_string(f) + " fps";
			f = frames;
			printf("%d fps\n", frames);
			frames = 0;
		}
	}
	delete texture;
	delete yaboi;

	FontManager::clear();

	return 0;
}

