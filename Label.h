#pragma once
#include "Renderables\Sprite.h"
#include "font_manager.h"
class Label : public Sprite
{
public:
	Font* _font;
	std::string text;
	float x, y;
	glm::vec4& position;
public:
	Label(std::string text, float x, float y, Font* font, glm::vec4 color);
	Label(std::string text, float x, float y,const std::string &font, glm::vec4 color);
	Label(std::string text, float x, float y, const std::string &font, unsigned int size, glm::vec4 color);

	void submit(Renderer2D* renderer) const override;
	void validateFont(const std::string& name, int size = -1) ;


};