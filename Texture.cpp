#include "Texture.h"

Texture::Texture(const char *path)
	: _path(path)
{
	_TexID = loadAndBufferImage(_path);
}

Texture::~Texture()
{

}

void Texture::bind() const
{
	glBindTexture(GL_TEXTURE_2D, _TexID);
}

void Texture::unbind() const
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::loadAndBufferImage(const char *filename)
{
	//SETTING UP TEXTURE BUFFER OBJECT

	GLuint resultTex;
	glGenTextures(1, &resultTex);
	glBindTexture(GL_TEXTURE_2D, resultTex);



	//LOADING THE DANG IMAGE M'DUDE (FOR THE TEXTURE)
	int img_width, img_height;
	unsigned char* image =
		SOIL_load_image(filename, &img_width, &img_height, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);


	//more texture setup stuff
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//specifying texture interpolation
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindTexture(GL_TEXTURE_2D, 0);

	return resultTex;
}