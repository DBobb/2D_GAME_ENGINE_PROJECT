#include "Label.h"

Label::Label(std::string text, float x, float y, Font* font, glm::vec4 color)
	: Sprite(), text(text), position(_position), _font(font)
{
	_position = glm::vec4(x, y, 0.0f, 1.0f);
	_color = color;
	

}	

Label::Label(std::string text, float x, float y, const std::string& font, glm::vec4 color)
	: Sprite(), text(text), position(_position), _font(FontManager::get(font))
{
	_position = glm::vec4(x, y, 0.0f, 1.0f);
	_color = color;

	validateFont(font);
}

Label::Label(std::string text, float x, float y, const std::string& font, unsigned int size, glm::vec4 color)
	: Sprite(), text(text), position(_position), _font(FontManager::get(font, size))
{
	_position = glm::vec4(x, y, 0.0f, 1.0f);
	_color = color;

	validateFont(font,size);


}

void Label::submit(Renderer2D* renderer) const
{
	renderer->drawString(text, _position, *_font, _color);
}

void Label::validateFont(const std::string& name, int size) 
{
	if (_font != nullptr)
		return;
	
		std::cout << "NULL FONT! Font = " << name;
		if (size > 0)
			std::cout << ", Size=" << size;
		std::cout << std::endl;

		_font = FontManager::get("Arial");
	
}