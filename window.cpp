#include "window.h"
#include "Renderables/Sprite.h"
//#include "Renderer2D.h"
#include "Renderers/BasicRenderer.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>


//ORIGINAL WINDOW STUFF
static void key_callback(GLFWwindow* g_window, int key, int scancode, int action, int mods)
{
	

	window* win = (window*) glfwGetWindowUserPointer(g_window);

	 win->keys[key] = (action != GLFW_RELEASE);
	

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(g_window, GLFW_TRUE);

	
}

static void mouse_button_callback(GLFWwindow* g_window, int button, int action, int mods)
{
	window* win = (window*)glfwGetWindowUserPointer(g_window);

	win->buttons[button] = (action != GLFW_RELEASE);

}

static void cursor_position_callback(GLFWwindow* g_window, double xpos, double ypos)
{
	window* win = (window*)glfwGetWindowUserPointer(g_window);
	win->mx = xpos;
	win->my = ypos; 
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void window_resize(GLFWwindow* g_window, int width, int height)
{
	glViewport(0, 0, width, height);
	window* win = (window*)glfwGetWindowUserPointer(g_window);
	win->w_width = width;
	win->w_height = height;
}

bool window::Init()
{
	if (!glfwInit())
	{
		return false;	
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	gameWindow = glfwCreateWindow(w_width, w_height, w_title, NULL, NULL);
	glfwSetKeyCallback(gameWindow, key_callback);
	glfwSetWindowSizeCallback(gameWindow, window_resize);
	glfwSetMouseButtonCallback(gameWindow, mouse_button_callback);
	glfwSetCursorPosCallback(gameWindow, cursor_position_callback);
	glfwSetFramebufferSizeCallback(gameWindow, framebuffer_size_callback);
	glfwSetWindowUserPointer(gameWindow, this);
	//glfwSwapInterval(0.0);

	FontManager::add(new Font("Arial", "arial.ttf", 32));

	//Initializing input array
	for (int i = 0; i < MAX_KEYS; i++)
	{
		keys[i] = false;
		keyState[i] = false;
		keyTyped[i] = false;
	}

	for (int i = 0; i < MAX_BUTTONS; i++)
	{
		buttons[i] = false;
		mouseState[i] = false;
		mouseClicked[i] = false;
	}

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	glfwMakeContextCurrent(gameWindow);

	INSTANCE = this;
	//Must set context before initializing glew.
	//Initializing GLEW
	if (glewInit() != GLEW_OK)
	{
		glfwTerminate();
		return false;
	}

	return true;

}

window::window(const char* title, int width, int height)
{
	w_title = title;
	w_width = width;
	w_height = height;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Init();

	glViewport(0, 0, w_width, w_height);
	
}

GLuint window::loadAndBufferImage(const char *filename)
{
	//SETTING UP TEXTURE BUFFER OBJECT
	
	GLuint texA;
	glGenTextures(1, &texA);
	glBindTexture(GL_TEXTURE_2D, texA);
	


	//LOADING THE DANG IMAGE M'DUDE (FOR THE TEXTURE)
	int img_width, img_height;
	unsigned char* image =
		SOIL_load_image(filename, &img_width, &img_height, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);


	//more texture setup stuff
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//specifying texture interpolation
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return texA;
}


void window::clear(float x, float y, float z)
{
	glClearColor(x, y, z, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


}

void window::update() 
{
	int width, height;
	glfwGetFramebufferSize(gameWindow, &width, &height);

	
	for (int i = 0; i < MAX_KEYS; i++)
		 keyTyped[i] = keys[i] && !keyState[i];

	for (int i = 0; i < MAX_BUTTONS; i++)
		mouseClicked[i] = buttons[i] && !mouseState[i];

	//Basically allows us to copy the contents of our keys array to the keystate array.  Saving us the time of iterating through and setting each thing individually.
	//Using this optimization to save computation time since these arrays will be set more or less every update.
	memcpy(keyState, keys, MAX_KEYS);
	memcpy(mouseState, buttons, MAX_BUTTONS);




	GLenum error = glGetError();

	//CHECKING FOR OPENGL ERRORS
	if (error != GL_NO_ERROR)
		std::cout << "OpenGL Error: " << error << std::endl;

	glfwPollEvents();
	glfwSwapBuffers(gameWindow);
}


window::~window()
{
	glfwTerminate();
	glfwDestroyWindow(gameWindow);
}

bool window::isKeyPressed(unsigned int keycode) const
{
	if (keycode >= MAX_KEYS)
		return false;
		
	return keys[keycode];
}

bool window::isKeyTyped(unsigned int keycode) const
{
	if (keycode >= MAX_KEYS)
		return false;

	return keyTyped[keycode];
}

bool window::isMouseButtonPressed(unsigned int button) const
{
	if (button >= MAX_BUTTONS)
		return false;

	return buttons[button];
}

bool window::isMouseButtonClicked(unsigned int button) const
{
	if (button >= MAX_BUTTONS)
		return false;

	return mouseClicked[button];
}

glm::vec2 window::getMousePosition() const
{
	glm::vec2 position(mx,WINDOW_HEIGHT - my);

	return position;
}