#pragma once

#include <vector>
#include "font.h"

class FontManager
{
private:
	static std::vector<Font*> _Fonts;

public:
	static void add(Font* font);
	static Font* get(const std::string& name);
	static Font* get(const std::string& name, unsigned int size);
	static void clear();
private:
	FontManager();
};