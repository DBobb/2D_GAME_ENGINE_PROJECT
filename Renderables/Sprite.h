#ifndef SPRITE_H
#define SPRITE_H

#include <iostream>
//#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../Buffers/VertexArray.h"
#include "../Buffers/Buffer.h"
#include "../Buffers/ElementBuffer.h"

#include "../Shaders.h"
#include "../Renderers/Renderer2D.h"
#include "../Texture.h"

struct VertexData
{
	glm::vec2 _vertex;
	glm::vec2 _texCoords;
	glm::vec4 _color;
	

	float _texID;
	float _text;

};

class Sprite
{
public:
	glm::vec4 _position;
	GLuint _textureBufferID;
	glm::vec3 _velocity;
	glm::vec4 _color;
	glm::vec2 _size;
protected:
	std::vector<glm::vec2> _texCoords;
	Texture* _texture;
protected:
	Sprite() : _texture(nullptr)
	{
		setUVDefault();

	}
public:
	Sprite(glm::vec4 position, glm::vec2 size,glm::vec4 color)
		: _position(position), _size(size), _color(color), _texture(nullptr)
	{
		setUVDefault();
	}
public:
	void setPosition(glm::vec4 newPosition);
	virtual ~Sprite();
	glm::vec3 getVelocity();
	void setVelocity(glm::vec3 newVelocity);
	virtual void submit(Renderer2D* renderer) const
	{
		renderer->submit(this);
	}

	void setColor(const glm::vec4& color) { _color = color; }

	inline const glm::vec4 getPosition() const { return _position; }
	inline const glm::vec4 getColor() const { return _color; }
	inline const glm::vec2 getSize() const { return _size; }
	inline const std::vector<glm::vec2>& getTexCoords() const { return _texCoords; }
	//inline const GLuint getTexBufferID() const{ return _textureBufferID; }
	inline const GLuint getTexID() const { return _texture == nullptr ? 0 : _texture->getID(); }

//	inline const VertexArray* getVAO() const { return _vao; }
//	inline const ElementBuffer* getEBO() const { return _ebo; }

//	inline Shaders& getShader() const { return _currShader; }

	

	void render();
	void update();

private:
	
	//Used to set texture coordinate defaults but writing texCoords was getting annoying so I made the swithc to UV. Which are pretty much x and y, but for like textures ya know?
	void setUVDefault()
	{
		_texCoords.push_back(glm::vec2(0, 0));
		_texCoords.push_back(glm::vec2(0, 1));
		_texCoords.push_back(glm::vec2(1, 1));
		_texCoords.push_back(glm::vec2(1, 0));
	}
};

#endif