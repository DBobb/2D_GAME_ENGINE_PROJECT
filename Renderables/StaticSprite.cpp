#include "StaticSprite.h"

StaticSprite::StaticSprite(float x, float y, float width, float height, glm::vec4& color, GLuint textureBufferID, Shaders& shader)
	: Sprite(glm::vec4(x, y, 0, 1), glm::vec2(width, height), color), _currShader(shader), _textureBufferID(textureBufferID)
{
	//_textureBufferID = textureBufferID;
	//_position = position;
	//_color = color;
	//_size = size;


	_vao = new VertexArray();

	GLfloat vertices[] = {	//color coordinates	  //texture coordinates
		0.0f,  0.0f,    // Top-left
		width,  0.0f,  // Top-right
		width, height, // Bottom-right
		0.0f, height   // Bottom-left
	};

	GLfloat colors[] = {
		color[0], color[1], color[2], color[3],
		color[0], color[1], color[2], color[3],
		color[0], color[1], color[2], color[3],
		color[0], color[1], color[2], color[3]
	};

	GLfloat texCoords[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	GLint elements[] = {
		0, 1, 2,
		2, 3, 0
	};

	_ebo = new ElementBuffer(elements, 6);

	//CREATING SEVERAL BUFFERS OF DATA TO ADD TO VAO (VERTEX ARRAY OBJECT)
	Buffer* vbo = new Buffer(vertices, 4 * 2, 2);
	Buffer* cbo = new Buffer(colors, 4 * 3, 3);
	Buffer* tcbo = new Buffer(texCoords, 4 * 2, 2);

	//ADDING BUFFERS TO VAO
	_vao->addBuffer(vbo, 0);
	_vao->addBuffer(cbo, 1);
	_vao->addBuffer(tcbo, 2);

}

StaticSprite::~StaticSprite()
{
	delete _vao;
	delete _ebo;
}