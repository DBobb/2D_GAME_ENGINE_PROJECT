#pragma once
#include "Sprite.h"

class  StdSprite : public Sprite
{
private:

public:
	StdSprite(float x, float y, float width, float height, const glm::vec4& color);
	StdSprite(float x, float y, float width, float height, Texture* texture);
};