#pragma once
#pragma once
#include "Sprite.h"

class  StaticSprite : public Sprite
{
private:
	VertexArray* _vao;
	ElementBuffer* _ebo;
	Shaders& _currShader;
	GLuint _textureBufferID;
public:
	StaticSprite(float x, float y, float width, float height, glm::vec4& color, GLuint textureBufferID, Shaders& shader);
	~StaticSprite();
	inline const VertexArray* getVAO() const { return _vao; }
	inline const ElementBuffer* getEBO() const { return _ebo; }

	inline Shaders& getShader() const { return _currShader; }
	inline const GLuint getTexBufferID() const { return _textureBufferID; }
};