#include "StdSprite.h"

StdSprite::StdSprite(float x, float y, float width, float height, const glm::vec4& color)
	: Sprite(glm::vec4(x, y, 0, 1), glm::vec2(width, height), color)
{

}

StdSprite::StdSprite(float x, float y, float width, float height, Texture* texture)
	: Sprite(glm::vec4(x, y, 0, 1), glm::vec2(width, height), glm::vec4(1,1,1,1))
{
	_texture = texture;
}