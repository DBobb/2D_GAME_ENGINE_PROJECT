#include "Sprite.h"

void Sprite::setPosition(glm::vec4 newPosition)
{
	_position = newPosition;
}



Sprite::~Sprite()
{
}

glm::vec3 Sprite::getVelocity()
{
	return _velocity;
}

void Sprite::setVelocity(glm::vec3 newVector)
{
	_velocity = newVector;
}

void Sprite::render()
{
	glBindTexture(GL_TEXTURE_2D, _textureBufferID);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


}
void Sprite::update()
{
	glm::mat4 translate = glm::translate(glm::mat4(1.f), _velocity);
	_position = translate * _position;
	std::cout << _position.x << " " << _position.y << " " << _position.z << std::endl;
}
