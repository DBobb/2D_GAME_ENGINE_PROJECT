#pragma once

#include "../Layers/Layer.h"
#include "../Renderers/BatchRenderer.h"

class TileLayer : public Layer
{	
public:
	TileLayer(Shaders* shader);
	virtual ~TileLayer();
};