#include "Group.h"

Group::Group(const glm::mat4& transform)
	: _TransformMat(transform)
{

}

Group::~Group()
{
	for (int i = 0; i < _Sprites.size(); i++)
	{
		delete _Sprites[i];
	}
}

void Group::submit(Renderer2D* renderer) const
{
	renderer->stackPush(_TransformMat);
	for (const Sprite* sprite : _Sprites)
		sprite->submit(renderer);
	renderer->stackPop();
}

void Group::add(Sprite* sprite)
{
	_Sprites.push_back(sprite);
}