#pragma once

#include "../Renderables/Sprite.h"

class Group : public Sprite
{
private:
	std::vector<Sprite*> _Sprites;
	glm::mat4 _TransformMat;
public:
	Group(const glm::mat4& transform);
	~Group();
	void submit(Renderer2D* renderer) const override;
	void add(Sprite* sprite);
};