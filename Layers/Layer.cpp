#include "Layer.h"

Layer::Layer(Renderer2D* renderer, Shaders* shader, glm::mat4 projectionMatrix)
	: _renderer(renderer), _shader(shader), _projMat(projectionMatrix)
{
	

	_shader->Use();
	_shader->setUniformMat4("projection", _projMat);

}

Layer::~Layer()
{
	//delete _shader;
	delete _renderer;

	for (int i = 0; i < _Sprites.size(); i++)
		delete _Sprites[i];
}

void Layer::add(Sprite* sprite)
{
	_Sprites.push_back(sprite);
}

void Layer::draw()
{

	_shader->Use();
	_renderer->begin();

	
	
	//_renderer->drawString("Nobody cares...", glm::vec4(5, 250, 0, 1), glm::vec4(1, 1, 1, 1));
	//_renderer->drawString("Also,", glm::vec4(5, 150, 0, 1), glm::vec4(1, 1, 1, 1));
	//_renderer->drawString("This was a massive waste of time.", glm::vec4(5, 50, 0, 1), glm::vec4(1, 1, 1, 1));
	
	
	int i = 0;
	for (const Sprite* sprite : _Sprites)
	{ 
		sprite->submit(_renderer);
	}

	

	_renderer->end();
	_renderer->flush();
	
}