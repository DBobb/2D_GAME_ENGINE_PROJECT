#pragma once

#include "../Renderers/Renderer2D.h"
#include "../Renderables/Sprite.h"

class Layer
{
private:
	Renderer2D* _renderer;
	std::vector<Sprite*> _Sprites;
	Shaders* _shader;
	glm::mat4 _projMat;

protected:
	Layer(Renderer2D* renderer, Shaders* shader, glm::mat4 projectionMatrix);
public:
	//Layer();
	virtual ~Layer();
	virtual void add(Sprite* sprite);
	void draw();

	inline const std::vector<Sprite*>& getSprites() const { return _Sprites; } ;
};