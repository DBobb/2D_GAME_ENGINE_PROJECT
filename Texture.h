#pragma once
#include <GL/glew.h>
#include <string>
#include "SOIL2\SOIL2.h"
class Texture
{
private:
	const char *_path;
	GLuint _TexID;
	unsigned int _width, _height;
public:
	Texture(const char *path);
	~Texture();
	void bind() const;
	void unbind() const;
	GLuint loadAndBufferImage(const char *filename);
	inline unsigned int getWidth() const { return _width; }
	inline unsigned int getHeight() const { return _height; }
	inline const unsigned int getID() const { return _TexID; }



};