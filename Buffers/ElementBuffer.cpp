#include "ElementBuffer.h"

ElementBuffer::ElementBuffer(GLint* data, GLsizei count)
{

	glGenBuffers(1, &_BufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _BufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(data), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}
ElementBuffer::~ElementBuffer()
{
	glDeleteBuffers(1, &_BufferID);
}
void ElementBuffer::bind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _BufferID);
}
void ElementBuffer::unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}