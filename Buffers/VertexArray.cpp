
#include "VertexArray.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &_VertArrayID);
}
VertexArray::~VertexArray()
{
	glDeleteBuffers(1, &_VertArrayID);
}

void VertexArray::addBuffer(Buffer* buffer, GLuint index) //Index corressponds to index in "layout X" in shader code
{
	bind();
	buffer->bind();

	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, buffer->getNumComponents(), GL_FLOAT, GL_FALSE, 0, 0);

	buffer->unbind();

}
void VertexArray::bind() const
{
	glBindVertexArray(_VertArrayID);
}
void VertexArray::unbind() const
{
	glBindVertexArray(0);
}