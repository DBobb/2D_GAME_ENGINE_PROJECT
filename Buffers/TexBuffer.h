#pragma once
#ifndef TEXBUFFER_H
#define TEXBUFFER_H

#include "GL/glew.h"

class TexBuffer
{
public:
	TexBuffer();
	void bind() const;
	void unbind() const;
private:
	GLuint _TexBufferID;     //The ID of whatever buffer we make
	//GLuint _NumComponents;   //The buffers number of components
};

#endif

