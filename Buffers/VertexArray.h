#pragma once

#include "GL/glew.h"
#include "Buffer.h"
#include <vector>

class VertexArray
{
private:
	GLuint _VertArrayID;		   //The ID used to track the data
	std::vector<Buffer*> _Buffers; //A vector of buffers

public:
	VertexArray();
	~VertexArray();

	void addBuffer(Buffer* buffer, GLuint index);
	void bind() const;
	void unbind() const;

};