#include "TexBuffer.h"

TexBuffer::TexBuffer()
{

	glGenBuffers(1, &_TexBufferID);
	glBindBuffer(GL_TEXTURE_2D, _TexBufferID);
	glBindBuffer(GL_TEXTURE_2D, 0);

}
void TexBuffer::bind() const
{
	glBindBuffer(GL_TEXTURE_2D, _TexBufferID);
}
void TexBuffer::unbind() const
{
	glBindBuffer(GL_TEXTURE_2D, 0);
}