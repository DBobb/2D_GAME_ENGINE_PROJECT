#include "Buffer.h"

Buffer::Buffer(GLfloat* data, GLsizei count, GLuint numComponents)
{
	_NumComponents = numComponents;
	glGenBuffers(1, &_BufferID);
	glBindBuffer(GL_ARRAY_BUFFER, _BufferID);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(GLfloat), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
Buffer::~Buffer()
{
	glDeleteBuffers(1, &_BufferID);
}
void Buffer::bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, _BufferID);
}
void Buffer::unbind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint Buffer::getNumComponents() const
{
	return _NumComponents;
}