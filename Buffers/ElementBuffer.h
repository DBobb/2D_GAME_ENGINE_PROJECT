#pragma once
#ifndef ELEMENTBUFFER_H
#define ELEMENTBUFFER_H

#include <GL/glew.h>

class ElementBuffer
{
public:
	ElementBuffer(GLint* data, GLsizei count);
	~ElementBuffer();
	void bind() const;
	void unbind() const;
	inline GLuint getNumComponents() const { return _NumElements; }
private:
	GLuint _BufferID;      //The ID of whatever buffer we make
	GLuint _NumElements;   //The buffers number of components
};

#endif