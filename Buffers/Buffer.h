#pragma once
#ifndef BUFFER_H
#define BUFFER_H

#include <GL/glew.h>

class Buffer
{
private:
	GLuint _BufferID;        //The ID of whatever buffer we make
	GLuint _NumComponents;   //The buffers number of components
public:
	Buffer(GLfloat* data, GLsizei count, GLuint numComponents);
	~Buffer();
	void bind() const;
	void unbind() const;
	GLuint getNumComponents() const;


};

#endif