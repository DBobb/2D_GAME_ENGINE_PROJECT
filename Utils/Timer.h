#pragma once

#include <chrono>

class Timer
{
private:
	/*
	LARGE_INTEGER _start;
	double _Freq;
*/
	typedef std::chrono::high_resolution_clock HighResolutionClock;
	typedef std::chrono::duration<float, std::milli> milliseconds_type;
	std::chrono::time_point<HighResolutionClock>  _start;
public:
	Timer()
	{
		reset();
	}

	void reset()
	{
		_start = HighResolutionClock::now();
	}

	float elapsed()
	{
		return std::chrono::duration_cast<milliseconds_type>(HighResolutionClock::now() - _start).count() / 1000.0f;
	}
};