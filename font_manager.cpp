#include "font_manager.h"

std::vector<Font*> FontManager::_Fonts;

void FontManager::add(Font* font)
{
	_Fonts.push_back(font);
}

void FontManager::clear()
{
	for (int i = 0; i < _Fonts.size(); i++)
	{
		delete _Fonts[i];
	}
}

Font* FontManager::get(const std::string& name)
{
	for (Font* font : _Fonts) 
	{
		if (font->getName() == name)
			return font;
	}
	//Might want to return a default font if you can't find anything tho.
	return nullptr;
}

Font* FontManager::get(const std::string& name, unsigned int size)
{
	for (Font* font : _Fonts)
	{
		if (font->getSize() == size && font->getName() == name)
			return font;
	}
	//Might want to return a default font if you can't find anything tho.
	return nullptr;
}