#include "font.h"

Font::Font(std::string name, std::string filename, int size)
	: _name(name), _filename(filename), _size(size)
{
	_FTAtlas = ftgl::texture_atlas_new(512, 512, 1);
	_FTFont = ftgl::texture_font_new_from_file(_FTAtlas, 35, filename.c_str());
	//"comicbd.ttf"
}