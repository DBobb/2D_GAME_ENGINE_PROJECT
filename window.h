#ifndef WINDOW_H
#define WINDOW_H

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
//#include <GL/eglew.h>

// GLFW
#include <GLFW/glfw3.h>
#include "SOIL2\SOIL2.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Shaders.h"
#include "Renderables/Sprite.h"
#include "Renderers/BasicRenderer.h"
#include "font_manager.h"

#define MAX_KEYS 1024
#define MAX_BUTTONS 32
#define WINDOW_HEIGHT 600

class window
{
private:

	int w_width, w_height;
	const char* w_title;
	bool Init();
	window* INSTANCE;
	//Inputs
	bool keys[MAX_KEYS];
	bool keyState[MAX_KEYS];
	bool keyTyped[MAX_KEYS];
	bool mouseState[MAX_BUTTONS];
	bool mouseClicked[MAX_BUTTONS];
	bool buttons[MAX_BUTTONS];
	double mx, my;

	//Vertex Array Obejct
	GLuint vao;
	//Texture ID
	GLuint texBufferID;
	
	
	Sprite* testSprite;
	BasicRenderer* testRenderer;
	//Sprite *roxy;
	//Shaders *currentShader;
	//Shaders newShader = Shaders("VertexShader2.sh", "FragShader2.sh");
	friend static void window_resize(GLFWwindow* window, int width, int height);
	friend static void key_callback(GLFWwindow* g_window, int key, int scancode, int action, int mods);
	friend static void mouse_button_callback(GLFWwindow* g_window, int button, int action, int mods);
	friend static void cursor_position_callback(GLFWwindow* g_window, double xpos, double ypos);

	//GLint viewLocation;
	//GLint modelLocation;
	//GLint projectionLoc;
   

public:
	GLuint loadAndBufferImage(const char *filename);
	GLFWwindow* gameWindow;
	window(const char* title, int width, int height);
	~window();
	void clear(float x, float y, float z);
	void update() ;
	bool close() { return glfwWindowShouldClose(gameWindow); }
	//void render();

	//Getters
	inline int getWidth() const { return w_width; }
	inline int getHeight() const { return w_height; }


	//INPUTS
	bool isKeyPressed(unsigned int keycode) const;
	bool isKeyTyped(unsigned int keycode) const;
	bool isMouseButtonPressed(unsigned int button) const;
	bool isMouseButtonClicked(unsigned int button) const;
	glm::vec2 getMousePosition() const;

};
#endif;
