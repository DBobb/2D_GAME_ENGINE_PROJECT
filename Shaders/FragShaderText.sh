#version 330 core
    in vec4 Color;
	in vec2 Texcoord;
	in vec2 pos;
	in float TexID;

    layout (location = 0) out vec4 outColor;
	
	uniform sampler2D textures[32];
	uniform vec2 light_pos;


    void main()
    {
		float intensity = 50.0 / length(pos.xy - light_pos);
		//outColor = Color * intensity;

		vec4 texColor = Color;
		if (TexID > 0.0)
		{
			int tid = int(TexID - 0.5); //Adding 0.5 to make sure the id is precise, basically flooring the value.
			texColor = texture(textures[tid], Texcoord);
			//texColor = vec4(tid, 0,0,1);
		}

       // outColor = vec4(1,0,1,texColor.r);// * intensity;
	   outColor = vec4(texColor.r, texColor.g, texColor.b,texColor.r);

    }