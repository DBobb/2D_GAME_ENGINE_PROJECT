#version 330 core


    layout (location = 0) in vec2 position;
	layout (location = 1) in vec2 texcoord;
	layout (location = 2) in float texID;
	layout (location = 3) in vec4 color;
	layout (location = 4) in float text;

    out vec4 Color;
	out vec2 Texcoord;
	out float TexID;
	out float Text;

	uniform mat4 model = mat4(1.0);
	uniform mat4 view = mat4(1.0);
	uniform mat4 projection = mat4(1.0);

	uniform mat4 transform = mat4(1.0);

	out vec2 pos; 

    void main()
    {
        Color = color;
		pos = position;
		TexID = texID;
		Text = text;
		Texcoord = vec2(texcoord.x, 1.0 - texcoord.y);
        gl_Position = projection * view * model vec4(position, 0.0, 1.0);
		
    }