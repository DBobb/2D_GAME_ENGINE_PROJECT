#version 150 core

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texA;
uniform sampler2D texB;
uniform float tick;

void main()
{
	vec4 colElijah = texture(texA,Texcoord);
	vec4 colMeme = texture(texB,Texcoord);

	float factor = (sin(tick * 3.0) + 1.0) / 2.0;
    //outColor = mix(colElijah,colMeme, factor);
	if (Texcoord.y < 0.5)
		outColor = texture(texA, Texcoord);
	else
		outColor = texture(texA, vec2(Texcoord.x + sin(Texcoord.y * 60.0 + tick * 2.0) / 30.0,  1.0 - Texcoord.y)) * vec4(0.7,0.7,1.0,1.0);
}


