#pragma once

#include "../External Libraries/Freetype-gl/freetype-gl-master/freetype-gl.h"
#include <string>
class Font
{
private:
		ftgl::texture_atlas_t* _FTAtlas;
		ftgl::texture_font_t* _FTFont;
		unsigned int _size;
		std::string _name;
		std::string _filename;

public:
	Font(std::string name, std::string filename, int size);

	inline ftgl::texture_font_t* getFTFont() const { return _FTFont; }
	

	inline const unsigned int getID() const { return _FTAtlas->id; }
	inline const std::string& getName() const { return _name; }
	inline const std::string& getFileName() const { return _filename; }
	inline const int getSize() const { return _size; }


};
